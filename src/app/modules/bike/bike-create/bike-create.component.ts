import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bike-create',
  templateUrl: './bike-create.component.html',
  styleUrls: ['./bike-create.component.css']
})
export class BikeCreateComponent implements OnInit {

  formBikeCreate: FormGroup;

  constructor(private formBuilder: FormBuilder) {

    this.formBikeCreate = this.formBuilder.group({
      model: ['', Validators.required],
      price: [''],
      serial: ['']
    });

   }

  ngOnInit() {
  }

  save(): void {
      console.warn('Datos',this.formBikeCreate.value);
  }

}
