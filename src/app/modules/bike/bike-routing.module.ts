import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BikeListComponent } from './bike-list/bike-list.component';
import { BikeCreateComponent } from './bike-create/bike-create.component';


const routes: Routes = [
  {
    path: 'bike-lists',
    component: BikeListComponent
  },
   {
     path: 'bike-create',
     component: BikeCreateComponent
   }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeRoutingModule { }
