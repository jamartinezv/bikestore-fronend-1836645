import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikeListComponent } from './bike-list/bike-list.component';
import { BikeRoutingModule } from './bike-routing.module';
import { BikeCreateComponent } from './bike-create/bike-create.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [BikeListComponent, BikeCreateComponent],
  imports: [
    CommonModule,
    BikeRoutingModule,
    ReactiveFormsModule
  ]
})
export class BikeModule { }
